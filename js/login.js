var paginaLogin = {
	loginPath : "/login",
	setAcaoBtnLogin : function (evento) {
		$(".btn-lg").on('click', paginaLogin.logar);
	},
	logar : function(e) {
		var login = $("input[name=email]").val();
		var senha = $("input[name=senha]").val();
		var urlRequest = url + paginaLogin.loginPath + "/logar";
		console.log("url: "+urlRequest);
		$.ajax({
			url : urlRequest,
			async : false,
			data : {
				"usuario" : login,
				"senha" : senha
			},
			statusCode : {
				200 : function(retorno) {
					paginaLogin.gravarSessao(retorno);
					window.location = urlSite+"/area-logada/home.html";
				},
				202 : function(retorno, textoStatus) {
					alert(textoStatus, "Alerta");
				},
				400 : function(resposta) {
					alert(resposta.responseText, "Erro");
				},
			},
			method : "POST"
		});
		e.preventDefault();
	},
	gravarSessao : function (usuario) {
		if (temSessao()) {
		    // Code for localStorage/sessionStorage.
		    localStorage.setItem("login", usuario);
		} else {
		    // Sorry! No Web Storage support..
		    alert("sem sessão no navegador");
		}
	}
	
};

paginaLogin.setAcaoBtnLogin();