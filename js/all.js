/**
 * 
 */

var all = {};
var JQuery = 'http://code.jquery.com/jquery-2.1.4.min.js';
var JQueryUi = 'http://code.jquery.com/ui/1.11.4/jquery-ui.js';
//var url = 'http://alexcaiom.ddns.net:83/crm-api';
var url = 'http://alexcaiom.ddns.net:83/crm-api';
var urlSite = 'http://alexcaiom.ddns.net/portal';
//var urlSite = 'http://localhost/portal';
var tagHead = document.getElementsByTagName('head')[0].innerHTML;
var ajaxAsyncPadrao = true;

function existe(o) {
	return o != null;
};

var naoExiste = function(o) {
	return !existe(o) || (typeof o === 'undefined');
};

var jaTemScript = function(fonte) {
	var tagHead = document.getElementsByTagName('head')[0];
	var tagsScript = tagHead.getElementsByTagName("script");
	
	for (var indice = 0; indice < tagsScript.length; indice++) {
		var tem = tagsScript[indice].src.indexOf(fonte) >= 0;
		if (tem) {
			return true;
		}
	}
	return false;
};

var importacaoJQuery = function(){
	if (!jaTemScript(JQuery)) {
		var tagScript = document.createElement('script');
		tagScript.setAttribute('src', JQuery);
		document.head.appendChild(tagScript);
	}
};
//importacaoJQuery();

var importacaoJQueryUi = function(){
	if (!jaTemScript(JQueryUi)) {
		var tagScript = document.createElement('script');
		tagScript.setAttribute('src', JQueryUi);
		document.head.appendChild(tagScript);
	}
};

/*all.scripts = [
    {
    	src : JQuery,
    	load : function() {
    		importacaoJQueryUi();
    	}
	},
	{
		src : JQueryUi,
		load : function() {
			
		}
	},
	
];
*/

var init = function(executar) {
	if (existe(executar) && typeof executar === 'function') {
		executar();
	}
//    all.formatarLinks();
};

function initMenu() {
	$( "#menu" ).menu();
    $( "#menu" ).removeClass("oculto");
}
//init(importacaoJQueryUi());


/*all.CONSEQUENCIAS = {
		SEM_ACESSO : 				'SEM_ACESSO',
		DESLOGADO_COM_SUCESSO : 	'DESLOGADO_COM_SUCESSO',
		ERRO : 						'ERRO',
		SUCCESSO : 					'SUCCESSO',
		MUITOS_ERROS : 				'MUITOS_ERROS',
		ATENCAO : 					'ATENCAO',
		TENTE_NOVAMENTE : 			'TENTE_NOVAMENTE',
		PARE_DE_TENTAR_NOVAMENTE =  'PARE_DE_TENTAR_NOVAMENTE'
};*/


all.formatarLinks = function() {
	$("a").on('click', function() {
		var url = $(this).attr('destino');  
		
		$.ajax({
			url : url,
			async : true
		})
		.done(function(retorno) {
			all.trocarConteudo(retorno);
		})
		.fail(function() {
			console.log(retorno);
		});
	});
};

all.trocarConteudo = function(conteudo) {
	$("#conteudo").html('');
	$("#conteudo").html(conteudo);
};

getValorValido = function(valor) {
	if (naoExiste(valor)) {
		return '';
	}
	return valor;
};

function log(texto) {
	console.log(texto);
};

function exibir(o) {
	if (existe(o)) {
		o.removeClass("oculto");
	}
};

function ocultar(o) {
	if (existe(o)) {
		o.addClass("oculto");
	}
};


$(window).ready(function() {generateWindowID()});
$(window).focus(function() {setAppId()});
$(window).mouseover(function() {setAppId()});

var ctNome = "CRM "

function generateWindowID() {
    //first see if the name is already set, if not, set it.
    if (window.name.indexOf(ctNome) == -1){
            window.name = ctNome + (new Date()).getTime();
    }
    setAppId()
}

function setAppId() {
    //generate the cookie
    strCookie = 'seAppId=' + window.name + ';';
    strCookie += ' path=/';

    if (window.location.protocol.toLowerCase() == 'https:'){
        strCookie += ' secure;';
    }

    document.cookie = strCookie;
}

function temSessao() {
	 return (typeof(Storage) !== "undefined");
}

function foto_usuario() {
	return "https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-1/p24x24/12647104_983469488367136_947488469539677282_n.jpg?oh=3cce6044ae2ec2cbc82c66a8659e4770&oe=5AEA3B9B"
}

var getUsuarioDaSessao = function() {
		if (temSessao()) {
		    // Code for localStorage/sessionStorage.
		    return localStorage.getItem("login");
		} else {
		    // Sorry! No Web Storage support..
		    return null;
		}
	}


var carregarElemento = function(elemento, pagina) {
	if (elemento.length > 0 && elemento.html().trim() === "") {
		$.ajax({
			url : pagina,
			async : false
		})
		.done(function(conteudo){
			elemento.html(conteudo);
		});
	}
}

var getNivelURL = function () {
	var urlAtual = window.location.href;
	var url = "";

	if(urlAtual.indexOf("area-logada") > -1) {
		url = getNivelURLAreaLogada(urlAtual);
	} else {
		url = getNivelURLAreaAberta(urlAtual);
	}

	
	return url;
}

var getNivelURLAreaLogada = function(urlAtual) {
	var numeroNiveis = urlAtual.split("/").length - 6;
	var url = "";

	for (var i = 0; i < numeroNiveis; i++) {
		url += "../";
	}
	return url;
}

var getNivelURLAreaAberta = function(urlAtual) {
	var raiz = urlAtual.substring(0, urlAtual.lastIndexOf("portal")+"portal".length);
	return raiz + "/area-logada/";
}

var carregarPainelUsuario = function() {
	carregarElemento($(".painel-usuario"), getNivelURL()+"./template/painel-usuario.html");
}

var carregarHeader = function() {
	carregarElemento($(".painel-header"), getNivelURL()+"./template/painel-header.html");
}

var carregarMenu = function() {
	carregarElemento($(".painel-menu"), getNivelURL()+"./template/painel-menu.html");
}

var carregarPaineis = function() {
	carregarHeader();
	carregarPainelUsuario();
	carregarMenu();
}

carregarPaineis();


