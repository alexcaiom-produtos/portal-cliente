var paginaUsuarios = {
    usuarioPath : "/usuario",
    carregarUsuarios : function() {
        //$("#data-table tbody").html("");
        var urlRequest = url + paginaUsuarios.usuarioPath + "?nome=";
        $.ajax({
            url : urlRequest,
            async : false,
            statusCode : {
                200 : function(retorno) {
                    paginaUsuarios.preencherLista(retorno);
                },
                202 : function(retorno, textoStatus) {
                    alert(textoStatus, "Alerta");
                },
                400 : function(resposta) {
                    alert(resposta.responseText, "Erro");
                },
            },
            method : "POST"
        });

            
    },
    preencherLista : function(usuarios) {
        $("#data-table tbody").html("");
        for (var i = 0; i < usuarios.length; i++) {
            $("#data-table tbody").html($("#data-table tbody").html()+paginaUsuarios.getLinhaDeTabela(usuarios[i]));
            //console.log(paginaUsuarios.getLinhaDeTabela(usuarios[i]));
        }
    },
    getLinhaDeTabela : function(usuario) {
        var linha = "<tr class=\"gradeA\" role=\"row\">";

        linha += "<td>" + usuario.nome + "</td>"
        linha += "<td>" + usuario.login + "</td>"
        linha += "<td>" + new Date(usuario.dataDeCriacao) + "</td>"
        linha += "<td>" + usuario.status + "</td>"
        linha += "<td>" + usuario.contadorSenhaInvalida + "</td>"
        linha += "<td>" + usuario.perfil.nivel + "</td>"
        linha += "<td>" + "</td>"

        linha += "</tr>";
        return linha;
    }
};